# Lab5 -- Integration testing


## Introduction

We've covered unit testing, which is widely used in indusry and is completely whitebox, but there are times when you need to use blackbox testing, for example if you cannot access code module, or if you need to check propper work of module, while calling it locally, so it returns correct values. That's what integration testing is being used for, it can be used both for whitebox and blackbox testing, depending on your goals and possibilities.   ***Let's roll!***🚀️

## Integration testing

Well, if unit testing is all about atomicity and independency, integration testing doesn't think like it, it is used less often then unit testing because it requires more resourses, and I haven't met a team yet, where developers were doing integration tests, usually it is a task for the automated testing team, to check integration of one module with another. Main goal of integration testing is to check that two connected modules(or more, but usually two is enough, because it is less complicated to code and maintain)are working propperly.

## BVA

BVA is a top method for creating integration tests and blackbox tests, when using BVA you should take module(or few modules) and analyse inputs and theirs boudaries, and then pick values that are placed on the border of equivalence classes. To assure the correctness it is usually to chek one value in between of the boundaries just in case.


## Lab
You can find your links in table for spring 2023 [here](https://docs.google.com/spreadsheets/d/1YYFY0XtzODmYxZJDKaEfSPipa6OYL-ps9a6X0GTO9SU/edit#gid=147093893)

Ok, finally, we won't need to write any code today :). Hope you're happy)
1. Create your fork of the `
Lab5 - Integration testing
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/s23-lab5-integration-testing)
2. Try to use InnoDrive application:
- At first we need to check our default parameters, for it we will need some REST client preferrably(Insomnia, Postman or you may use your browser), install it. 
- Then let's read through the description of our system:
    The InnoDrive is a hypothetical car sharing service that provides transportations means in Kazan region including Innopolis. The service proposes two types of cars `budget` and `luxury`. The service offers flexible two tariff plans. With the `minute` plan, the service charges its client by `time per minute of use`. The Service also proposes a `fixed_price` plan whereas the price is fixed at the reservation time when the route is chosen. If the driver deviates from the planned route for more than `n%`, the tariff plan automatically switches to the `minute` plan. The deviation is calculated as difference in the distance or duration from the originally planned route. The `fixed_price` plan is available for `budget` cars only. The Innopolis city sponsors the mobility and offers `m%` discount to Innopolis residents.
    Imagine you are the quality engineer of Innodrive. How are you going to know about application quality???
- InnoDrive application is just a web application that allows you to compute the price for a certain car ride
- To get particularly your defaults, let's send this request:
    `https://script.google.com/macros/s/_your_key_/exec?service=getSpec&email=_your_email_` 
  This will return our default spec(your result might be different):
        Here is InnoCar Specs:

        Budet car price per minute = 17

        Luxury car price per minute = 33

        Fixed price per km = 11

        Allowed deviations in % = 10

        Inno discount in % = 10

- To get the price for certain car ride, let's send next request:
`https://script.google.com/macros/s/_your_key_/exec?service=calculatePrice&email=_your_email_&type=_type_&plan=_plan_&distance=_distance_&planned_distance=_planned_distance_&time=_time_&planned_time=_planned_time_&inno_discount=_discount_` with some random parameters. Answer should be like `{"price": 100}` or `Invalid Request`
- And next we can create our BVA table, this is a lab, so I will create it just for 2 parameters: 
`distance` and `type`.
 `distance` is an integer value, so we can build 2 equivalence classes:
 + `distance` <= 0
 + `distance` > 0
 while `type` have 3 equivalence classes:
 + `budget` 
 + `luxury`
 + or some `nonsense`.

- Let's use BVA to make integration tests,we need to make few different testcases with `distance`, depending on its value: 
 + `distance` <= 0 : -10 0
 + `distance` > 0 : 1 100000
This way we will test both our borders and our normal values, we can do the same thing for `type`:
 + `type` = "budget" 
 + `type` = "luxury" 
 + `type` = "Guten morgen sonnenschein" 

- Now, let's check responses for different values of our parameters, with all other parameters already setted up, so we will have:
  + `plan` = `minute` 
  + `planned_distance` = `100` 
  + `time` = `110` 
  + `planned_time` = `100` 
  + `inno_discount` = `yes`
Let's build test cases out of this data:

| Test case  | distance |   type    | Expected result      |
|------------|----------|-----------|----------------------|
|     1      |    -10   |     *     |   Invalid Request    |
|     2      |    0     |     *     |   Invalid Request    |
|     3      |    *     | "nonsense"|   Invalid Request    |
|     4      |    1     |"budget"   |   1683               |
|     5      |    1     |"luxury"   |   3267               |
|     6      | 1000000  |"budget"   |   1683               |
|     7      | 1000000  |"luxury"   |   3267               |

etc...(there are a lot of combinations), to pick only needed one it is a good practice to use specialized tools.
Let's use Decision table to cut out our tests:
| Conditions(inputs)  |             Values           |    R1    |   R2    |   R3  |    R4   |
|---------------------|------------------------------|----------|---------|-------|---------|
|     Type            | "budget","luxury", nonsense  | nonsense |  budget | luxury|    *    |
|     Distance        | >0, <=0                      |     *    |  >0     |  >0   |    <=0  |
|---------------------|------------------------------|----------|---------|-------|---------|
|  Invalid Request    |                              |    X     |         |       |    X    |
|     200             |                              |          |    X    |   X   |         |

Now let's use this request to test our values




## Homework

As a homework you will need to develop BVA and Decision table for your service, guaranteed that your service has a bug somewhere(maybe even few), you need to catch'em all using BVA. Submit your catched bugs and Tables, adding them to the Readme of your branch. 

Also you have to provide some kind of estimation of coverage you have done

Note: You can use any automated solution, but in this case I can ask whether you are sure that this solution does not contain the same bugs

Note 2: 10% of lab grade would be soundness of bugs you have found(in realtion to total amount of bugs injected in your version)

## Solution

During the lab we conducted BVA and build Decision talbe for both `type` and `distance`. Now, I will do the same for all values. After I will create a decision table. Finally, I will compare the results, taking to consideration their "dependability". By "dependability" here I define relation between parameters, e.g. parameters such as `planned_distance` and `distance` influence the `plan`, such that it can go from `fixed_price` to `minute` plan.

As for my defaults I get next InnoCar Specs:
- Budet car price per minute = 25
- Luxury car price per minute = 58
- Fixed price per km = 21
- Allowed deviations in % = 5
- Inno discount in % = 16

Innodrive take next values to compute price:
+ type
+ plan
+ distance
+ planned_distance
+ time
+ planned_time
+ inno_discount
I will proceed with order defined above.

The parameter `plan` is a string value, so we can have 3 equivalence classes:
 + `minute`
 + `fixed_price`
 + or some `nonsense`

The `planned_distance` is like `distance`:
 + `planned_distance` < 0
 + `planned_distance` >= 0
 + `planned_distance` equals string

The `time` parameter has 3 equivalence classes:
 + `time` < 0
 + `time` >= 0
 + `time` equals string

Same for `planned_time`:
 + `planned_time` < 0
 + `planned_time` >= 0
 + `planned_time` equals string

The `inno_discount` can take:
 + `yes`
 + `no`
 + or some `nonsense`.

Taking this to consideration, I should have at least 2317 test cases. However, there are variables which has domain intersect, like `distance` with `planned_distance`, `time` with `planned_time`. Taking those into account there can even more.

Now, the decision table. When the decision table will be ready, I will conduct test cases to check if inputs match expected outputs. First, I listed all inputs and results. Then, I filled columns with all possible combinations. Afterward, specified the expected outputs.

|Conditions|            Values          |  R1  |  R2  |  ...  |
|----------|----------------------------|------|------|-------|
|   Type   | "budget","luxury", ?       |      |      |       |
|   Dist   | >0, <=0, ?                 |      |      |       |
|   Plan   | "minute", "fixed_price", ? |      |      |       |
|  PlDist  | >0, <=0, ?                 |      |      |       |
|   Time   | >0, <=0, ?                 |      |      |       |
|  PlTime  | >0, <=0, ?                 |      |      |       |
| Discount | "yes", "no", ?             |      |      |       |
|----------|----------------------------|------|------|-------|
| Invalid  |                            |      |      |       |
|  200     |                            |      |      |       |

I will write R1 and other results in row, to have space. If `type`, `plan` or `discount` is not specified I expect to see 'invalid' response.

| #R | Type | Dist | Plan | PlDist | Time | PlTime | Discount | Invalid | 200 |
|----|------|------|------|--------|------|--------|----------|---------|-----|
|  1 |   ?  |   *  |   *  |     *  |  *   |   *    |    *     |   yes   |     |
|  2 |   *  |   *  |   ?  |     *  |  *   |   *    |    *     |   yes   |     |
|  3 |   *  |   *  |   *  |     *  |  *   |   *    |    ?     |   yes   |     |

This variants leave me with 648 variants:
2317 - 1 * (3^6) - 1 * (3^5*2) - 1 * (3^4*2^2) = 648 tests (here and further I mean test cases)

If `type` is luxury, it does not matter which distance we go:

| #R |    Type   | Dist | Plan | PlDist | Time | PlTime | Discount | Invalid | 200 |
|----|-----------|------|------|--------|------|--------|----------|---------|-----|
|  4 |   luxury  |   *  |   *  |     *  |  <0  |   *    |    *     |   yes   |     |
|  5 |   luxury  |   *  |   *  |     *  | =>0  |   *    |    *     |         | yes |
|  6 |   luxury  |   *  |   *  |     *  |  *   |   *    |    *     |   yes   |     |

So, we left with 324 tests:
648 - 1 * (3*2*3*3*3*2) = 324 (logical)

For `minute` plan `time` only matters, since I expect to see next scenarios:

| #R |    Type   | Dist | Plan | PlDist | Time | PlTime | Discount | Invalid | 200 |
|----|-----------|------|------|--------|------|--------|----------|---------|-----|
|  7 |   budget  |   *  |minute|     *  | >=0  |   *    |    *     |         | yes |
|  8 |   budget  |   *  |minute|     *  |  ?   |   *    |    *     |   yes   |     |
|  9 |   budget  |   *  |minute|     *  |  <0  |   *    |    *     |   yes   |     |

Thus, 324 - 3^2*2*9 = 162 tests left

When `plan` is fixed: `distance`, `planned_distance`, `time`, and `planned_time` matter.
Depicting all 162 variants is not easy, since I will go via the cases which should work for sure:
if all inputs valid, I should have a valid output; if `distance` inputs are wrong when `time` inputs should be right;
if `time` inputs are wrong when `distance` inputs should be right. For all other cases it should be invalid.

| #R |    Type   | Dist   |   Plan    | PlDist | Time | PlTime | Discount | Invalid | 200 |
|----|-----------|--------|-----------|--------|------|--------|----------|---------|-----|
| 10 |   budget  |  <0,?  |fixed_price|     *  | >=0  |   ?    |    *     |   yes   |     |
| 11 |   budget  |  <0,?  |fixed_price|     *  | >=0  |   <0   |    *     |   yes   |     |
| 12 |   budget  |  <0,?  |fixed_price|     *  | >=0  |  >=0   |    *     |         | yes |
| 13 |   budget  |  <0,?  |fixed_price|     *  |  ?   |   *    |    *     |   yes   |     |
| 14 |   budget  |  <0,?  |fixed_price|     *  |  <0  |   *    |    *     |   yes   |     |

162 - 2*3*3*3*2 = 54 tests left

| #R |    Type   | Dist  |   Plan    | PlDist | Time | PlTime | Discount | Invalid | 200 |
|----|-----------|-------|-----------|--------|------|--------|----------|---------|-----|
| 15 |   budget  |  >=0  |fixed_price|   >=0  |   *  |    *   |    *     |         | yes |
| 16 |   budget  |  >=0  |fixed_price|    <0  |  >=0 |  <0,?  |    *     |   yes   |     |
| 17 |   budget  |  >=0  |fixed_price|    <0  |  >=0 |   >=0  |    *     |         | yes |
| 18 |   budget  |  >=0  |fixed_price|     ?  |  >=0 |  <0,?  |    *     |   yes   |     |
| 19 |   budget  |  >=0  |fixed_price|     ?  |  >=0 |   >=0  |    *     |         | yes |

54 - 3*3*2 - 3*2 - 3*2 = 24 tests left

| #R |    Type   | Dist  |   Plan    | PlDist | Time | PlTime | Discount | Invalid | 200 |
|----|-----------|-------|-----------|--------|------|--------|----------|---------|-----|
| 20 |   budget  |  >=0  |fixed_price|    <0  |   ?  |    *   |    *     |   yes   |     |
| 21 |   budget  |  >=0  |fixed_price|    <0  |  <0  |    *   |    *     |   yes   |     |
| 22 |   budget  |  >=0  |fixed_price|     ?  |   ?  |    *   |    *     |   yes   |     |
| 23 |   budget  |  >=0  |fixed_price|     ?  |  <0  |    *   |    *     |   yes   |     |

24 - 2*3*2 - 2*3*2 = 0 (just for check)
As intendent all other cases should be invalid, now the final decision table will look like this:



|Conditions|    Type   |  Dist  |   Plan    | PlDist | Time | PlTime | Discount | Invalid | 200 |
|----|-----------|--------|-----------|--------|------|--------|----------|---------|-----|
|values|?,luxury,budget|?,<0,>=0|?,minute,fixed_price|?,<0,>=0|?,<0,>=0|?,<0,>=0|?,yes,no|         |     |


| #R |    Type   |  Dist  |   Plan    | PlDist | Time | PlTime | Discount | Invalid | 200 |
|----|-----------|--------|-----------|--------|------|--------|----------|---------|-----|
|  1 |      ?    |    *   |     *     |     *  |  *   |   *    |    *     |   yes   |     |
|  2 |      *    |    *   |     ?     |     *  |  *   |   *    |    *     |   yes   |     |
|  3 |      *    |    *   |     *     |     *  |  *   |   *    |    ?     |   yes   |     |
|  4 |   luxury  |    *   |     *     |     *  |  <0  |   *    |    *     |   yes   |     |
|  5 |   luxury  |    *   |     *     |     *  | =>0  |   *    |    *     |         | yes |
|  6 |   luxury  |    *   |     *     |     *  |  *   |   *    |    *     |   yes   |     |
|  7 |   budget  |    *   |   minute  |     *  | >=0  |   *    |    *     |         | yes |
|  8 |   budget  |    *   |   minute  |     *  |  ?   |   *    |    *     |   yes   |     |
|  9 |   budget  |    *   |   minute  |     *  |  <0  |   *    |    *     |   yes   |     |
| 10 |   budget  |  <0,?  |fixed_price|     *  | >=0  |   ?    |    *     |   yes   |     |
| 11 |   budget  |  <0,?  |fixed_price|     *  | >=0  |   <0   |    *     |   yes   |     |
| 12 |   budget  |  <0,?  |fixed_price|     *  | >=0  |  >=0   |    *     |         | yes |
| 13 |   budget  |  <0,?  |fixed_price|     *  |  ?   |   *    |    *     |   yes   |     |
| 14 |   budget  |  <0,?  |fixed_price|     *  |  <0  |   *    |    *     |   yes   |     |
| 15 |   budget  |   >=0  |fixed_price|   >=0  |   *  |    *   |    *     |         | yes |
| 16 |   budget  |   >=0  |fixed_price|    <0  |  >=0 |    ?   |    *     |   yes   |     |
| 17 |   budget  |   >=0  |fixed_price|    <0  |  >=0 |    <0  |    *     |   yes   |     |
| 18 |   budget  |   >=0  |fixed_price|    <0  |  >=0 |   >=0  |    *     |         | yes |
| 19 |   budget  |   >=0  |fixed_price|     ?  |  >=0 |    ?   |    *     |   yes   |     |
| 20 |   budget  |   >=0  |fixed_price|     ?  |  >=0 |    <0  |    *     |   yes   |     |
| 21 |   budget  |   >=0  |fixed_price|     ?  |  >=0 |   >=0  |    *     |         | yes |
| 22 |   budget  |   >=0  |fixed_price|    <0  |   ?  |    *   |    *     |   yes   |     |
| 23 |   budget  |   >=0  |fixed_price|    <0  |  <0  |    *   |    *     |   yes   |     |
| 24 |   budget  |   >=0  |fixed_price|     ?  |   ?  |    *   |    *     |   yes   |     |
| 25 |   budget  |   >=0  |fixed_price|     ?  |  <0  |    *   |    *     |   yes   |     |

Considering the decision talbe, I found next issues:
1. for **R#7** group tests I receive `invalid` response if `planned_time` is less than zero.
2. for **R#8** group tests I receive 200 response if `planned_time` is not less than zero (value is `Null`).
3. for **R#10** group tests I receive 200 response if `planned_time` is nonsense (should be invalid).
4. for **R#13** group tests I receive 200 response if `planned_time` is not less than zero (value is `Null`).

from **R#15** to **R#25** `distance` and `planned_distance` do not matter at all. Thus:

5. for **R#15** group tests fails, for negative values of `time` and `planned_time`, while 6. for 'nonsense' value equals null.
7. for **R#16** group tests I receive 200 response (`invalid` should be, value equals Null).
8. for **R#19** group tests I receive 200 response (`invalid` should be, value equals Null).
9. for **R#22** group tests I receive 200 response if `planned_distance` not less than zero or equals nonsense (response value Null).
10. for **R#24** group tests I receive 200 response if `planned_distance` not less than zero or equals nonsense (response value Null).

Those are the problems, however this list is not full. 
I did not consider cases of 'fixed_price'. In task descritption were said that plan stays in 'fixed_price'
if certain conditions are meet. This is true for both `time` and `distance`.
Considering InnoCar specs, the maximum `distance` allowed in `fixed_price` plan is
+ `distance` <= `planned_distance`*(1,05)
Other it will switch plan to `minute`
+ `distance` > `planned_distance`*(1,05)
Same for `time`
Again, I did not checked if calculation is done right in such constraints

Thank you for attention. 
